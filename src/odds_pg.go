package main

import (
	"fmt"
	"math"
	"strings"
)

type DecimalOdds float64
type AmericanOdds int64

func DecimalToAmericanWithRoundingDown(d DecimalOdds) AmericanOdds {
	if d < 2.00 {
		fmt.Printf("Value before rounding: %.2f\n", -100/(float64(d)-1))
		return AmericanOdds(math.Floor(-100 / (float64(d) - 1)))
	} else {
		fmt.Printf("Value before rounding: %.2f\n", (float64(d)-1)*100)
		return AmericanOdds(math.Floor((float64(d) - 1) * 100))
	}
}

func DecimalToAmericanWithNoRoundingDown(d DecimalOdds) AmericanOdds {
	if d < 2.00 {
		return AmericanOdds(math.Round(-100 / (float64(d) - 1)))
	} else {
		return AmericanOdds(math.Round((float64(d) - 1) * 100))
	}
}

func AmericanToDecimal(a AmericanOdds) DecimalOdds {
	if a < 0 {
		return DecimalOdds((a / 100) + 1)
	} else {
		return DecimalOdds((100 / a) + 1)
	}
}

func main() {

	//do some simple tests https://www.pinnacle.com/en/betting-articles/educational/converting-between-american-and-decimal-odds/PBS2VKQZ7ZB5TZDB

	d := DecimalOdds(1.99)
	expectedOdds := AmericanOdds(-101)
	testOdds(d, expectedOdds)

	d = DecimalOdds(1.952)
	expectedOdds = AmericanOdds(-105)
	testOdds(d, expectedOdds)

	d = DecimalOdds(1.909)
	expectedOdds = AmericanOdds(-110)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.000)
	expectedOdds = AmericanOdds(+100)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.030)
	expectedOdds = AmericanOdds(+103)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.050)
	expectedOdds = AmericanOdds(+105)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.060)
	expectedOdds = AmericanOdds(+106)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.10)
	expectedOdds = AmericanOdds(+110)
	testOdds(d, expectedOdds)

	d = DecimalOdds(2.231)
	expectedOdds = AmericanOdds(+110)
	testOdds(d, expectedOdds)
}

func testOdds(d DecimalOdds, expectedOdds AmericanOdds) {
	fmt.Println(strings.Repeat("*", 20))
	fmt.Printf("DecimalOdds=%v, Expected American Odds from Pinnacle=%d\n", d, expectedOdds)
	fmt.Printf("With Rounding down: %v\n", DecimalToAmericanWithRoundingDown(d))
	fmt.Printf("Without Rounding down: %v\n", DecimalToAmericanWithNoRoundingDown(d))
}
